#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/azure-aks-pipe"}

  echo "# Building image..." >&3
  run docker build -t ${DOCKER_IMAGE} .
  
  echo "# Create testing resources" >&3
  # required ${AZURE_AD_ADMIN_GROUP} THE_AZURE_APP_ID IS MEMBER OF

  AZURE_RESOURCE_GROUP="bbci-pipes-aks-test-${BITBUCKET_BUILD_NUMBER}-1"
  AZURE_AKS_NAME="bbci-pipes-aks-test-${BITBUCKET_BUILD_NUMBER}-1"

  echo "# login" >&3
  az login --service-principal --username ${AZURE_APP_ID}  --password ${AZURE_PASSWORD} --tenant ${AZURE_TENANT_ID}

  # the Azure subscription needs to have several providers enabled
  az provider register --namespace Microsoft.ContainerService
  az feature register --name UserAssignedIdentityPreview --namespace Microsoft.ContainerService
  az feature list -o table --query "[?contains(name, 'Microsoft.ContainerService/UserAssignedIdentityPreview')].{Name:name,State:properties.state}"
  az feature list -o table --query "[?contains(name, 'Microsoft.ContainerService')].{Name:name,State:properties.state}" | grep dentity

  # the Azure CLI (“az”) needs to have the aks-preview extension installed
  az extension list
  az extension add --name aks-preview
  az extension update --name aks-preview


  echo "# creating resource group ${AZURE_RESOURCE_GROUP}" >&3
  az group create --name ${AZURE_RESOURCE_GROUP} --location westus2

  echo "# creating AKS cluster ${AZURE_AKS_NAME}" >&3
  az aks create \
    --resource-group ${AZURE_RESOURCE_GROUP} \
    --name ${AZURE_AKS_NAME} \
    --node-count 1 \
    --enable-addons monitoring \
    --service-principal ${AZURE_APP_ID} \
    --client-secret ${AZURE_PASSWORD} \
    --generate-ssh-keys \
    --enable-aad \
    --enable-azure-rbac \
    --aad-tenant-id ${AZURE_TENANT_ID} \
    --aad-admin-group-object-ids ${AZURE_AD_ADMIN_GROUP}

  echo "# Creating role assignment" >&3
  AKS_ID=$(az aks show -g $AZURE_RESOURCE_GROUP -n $AZURE_AKS_NAME --query id -o tsv)
  az role assignment create --role "Azure Kubernetes Service RBAC Admin" --assignee $AZURE_APP_ID --scope $AKS_ID

}

teardown() {
    echo "# teardown - deleting resource group ${AZURE_RESOURCE_GROUP}" >&3
    az group delete -n ${AZURE_RESOURCE_GROUP} -y --no-wait
    
}

@test "deploy votefront" {
    #check that we can connect to the cluster

    echo "# connect to cluster" >&3
    
    run docker run \
        -e AZURE_APP_ID=${AZURE_APP_ID} \
        -e AZURE_PASSWORD=${AZURE_PASSWORD} \
        -e AZURE_TENANT_ID=${AZURE_TENANT_ID} \
        -e AZURE_AKS_NAME=${AZURE_AKS_NAME} \
        -e AZURE_RESOURCE_GROUP=${AZURE_RESOURCE_GROUP} \
        -e KUBECTL_COMMAND="version" \
        -e AZURE_LOGIN_NON_INTERACTIVE=true \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}

    echo "# Status: $status"
    echo "# Output: $output"

    [ "$status" -eq 0 ]
    [[ -n $(echo "$output" | grep 'Success') ]]

    # perform deployment
    echo "# perform deployment" >&3
    run docker run \
        -e AZURE_APP_ID=${AZURE_APP_ID} \
        -e AZURE_PASSWORD=${AZURE_PASSWORD} \
        -e AZURE_TENANT_ID=${AZURE_TENANT_ID} \
        -e AZURE_AKS_NAME=${AZURE_AKS_NAME} \
        -e AZURE_RESOURCE_GROUP=${AZURE_RESOURCE_GROUP} \
        -e KUBECTL_COMMAND="apply" \
        -e KUBECTL_ARGUMENTS="-f ${BATS_TEST_DIRNAME}/azure-vote.yaml" \
        -e AZURE_LOGIN_NON_INTERACTIVE=true \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}

    echo "# Status: $status"
    echo "# Output: $output"

    [ "$status" -eq 0 ]
    [[ -n $(echo "$output" | grep 'Success') ]]

    #get frontend service detail
    echo "# get frontend service detail" >&3
    declare -i COUNTER;
    while [[ -z $frontendip && $COUNTER -le 10 ]]
    do
        COUNTER+=1
        sleep 15s
        echo "# Attempt: $COUNTER" >&3

        run docker run \
        -e AZURE_APP_ID=${AZURE_APP_ID} \
        -e AZURE_PASSWORD=${AZURE_PASSWORD} \
        -e AZURE_TENANT_ID=${AZURE_TENANT_ID} \
        -e AZURE_AKS_NAME=${AZURE_AKS_NAME} \
        -e AZURE_RESOURCE_GROUP=${AZURE_RESOURCE_GROUP} \
        -e KUBECTL_COMMAND="get" \
        -e KUBECTL_ARGUMENTS="service azure-vote-front --output=jsonpath='{.status.loadBalancer.ingress[0].ip}'" \
        -e AZURE_LOGIN_NON_INTERACTIVE=true \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}

        echo "# Status: $status"
        echo "# Output: $output"
        
        [ "$status" -eq 0 ]
        
        frontendip=$(echo $output | sed -r '/\n/!s/[0-9.]+/\n&\n/;/^([0-9]{1,3}\.){3}[0-9]{1,3}\n/P;D')
        echo "# frontend ip: $frontendip" >&3
    done

    #assert ip not empty
    [ -n $frontendip ]
    
    echo "# Check website running at http://${frontendip}" >&3
}

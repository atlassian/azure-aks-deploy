FROM mcr.microsoft.com/azure-cli:2.70.0

SHELL ["/bin/sh", "-o", "pipefail", "-c"]

WORKDIR /

RUN tdnf install git-2.45.2 -y \
    && curl -fsSL -o /common.sh https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.6.0/common.sh \
    && az aks install-cli --client-version v1.32.0 --kubelogin-version v0.1.6

COPY pipe /
COPY LICENSE.txt README.md pipe.yml /
RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]

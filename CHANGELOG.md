# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.4.2

- patch: Internal maintenance: Bump base docker image to 2.70.0.
- patch: Internal maintenance: Update pipes versions in pipelines configuration file.

## 1.4.1

- patch: Internal maintenance: Bump pipes versions in pipelines configuration file.
- patch: Internal maintenance: Update docker image to azure-cli:2.67.0

## 1.4.0

- minor: Bump azure-cli to version 2.63.0.
- minor: Bump kubectl version to 1.31.0.
- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 1.3.0

- minor: Bump azure-cli to version 2.56.0.
- minor: Bump kubectl version to 1.29.1
- minor: Migrate to the Kubernetes community-owned repositories dl.k8s.io.
- patch: Internal maintenance: bump kubelogin to v0.1.0.

## 1.2.0

- minor: Add support to provide custom subscription with AZURE_SUBSCRIPTION variable.
- minor: Internal maintenance: Update pipe's docker image to mcr.microsoft.com/azure-cli:2.54.0.
- patch: Internal maintenance: Bump pipes and docker images versions in pipelines config file.

## 1.1.0

- minor: Update azure-cli image to version 2.39.0.
- minor: Update kubectl to version 1.25.0.
- patch: Internal maintenance: update bitbucket-pipes-toolkit-bash.
- patch: Internal maintenance: update community link.
- patch: Internal maintenance: update default image, pipe versions in bitbucket-pipelines.yml.

## 1.0.2

- patch: Internal maintenance: make pipe structure consistent to others.

## 1.0.1

- patch: Introduced kubelogin as optional feature to get around interactive device login

## 1.0.0

- major: Note: This pipe was forked from https://bitbucket.org/microsoft/azure-aks-deploy for future maintenance purposes.
